# Giotto Viewer

Giotto Viewer is a web-based visualization package for spatial transcriptomic data.

It interactively displays spatial gene expression data, and allows users to interact with the data in a way that would be difficult to do in a static visualization. Specifically, Giotto Viewer allows users to:

1.  create panels for a comparison of spatial annotations and cell type annotations for individual cells
2.  switch between displaying different annotations 
3.  intuitively select specific cells of interest
4.  display staining images and cell morphology information
5.  display transcript localization data

![alt text](viewer.small.png "Giotto Viewer logo")

## Demo of the Giotto Viewer

A demo of the visual cortex dataset (seqFISH+) with 523 cells, 10,000 genes has been created, and can be viewed by following this link [here](http://spatial.rc.fas.harvard.edu/morphology/leaflet.stitched.html).

## Components of Giotto Viewer

*  **giotto-viewer**: this current repository, serves as central introductory website. https://bitbucket.org/qzhudfci/giotto-viewer
*  **giotto-viewer.js**: Javascript package (main code). https://bitbucket.org/qzhudfci/giotto-viewer.js
*  **giotto-viewer-processing**: Python package for preparing staining images, preparing files for viewer. https://bitbucket.org/qzhudfci/giotto-viewer-processing

You will need to download the 2nd and 3rd to use Giotto Viewer.

## Pre-requisite software packages

*  giotto-analyzer: R package that interfaces with Giotto Viewer
*  Tile-up Ruby package
*  ImageJ library (JAR file)
*  ImageMagick

## Installation of pre-requisites 
Follow this link [INSTALL.md](INSTALL.md) to learn about installation instructions.

## Installation of Giotto Viewer

Once the prerequisite software have been installed, follow this link [TUTORIAL.md](tutorial.md) to get started with how to set up a dataset for Giotto Viewer.

## Tutorial for using Giotto Viewer

Follow this link [USAGE.md](usage.md) to learn how to navigate different functions within Giotto Viewer.
